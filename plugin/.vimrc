
" set appropriate paths
"set runtimepath=/media/dutch/media/Insync/Stuff/Config/vimfiles,$VIMRUNTIME
"let VIMCONFIGPATH=$HOME . '/media/dutch/media/Insync/Stuff/Config/vimfiles'

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.vim/plugged')

" Make sure you use single quotes
Plug 'junegunn/vim-easy-align'
Plug 'Lokaltog/vim-easymotion'
Plug 'sjl/gundo.vim'
Plug 'jlanzarotta/bufexplorer'
Plug 'uguu-org/vim-matrix-screensaver'
Plug 'altercation/vim-colors-solarized'
Plug 'vim-scripts/WhereFrom'

" Plug 'Rykka/riv.vim'
" Plug 'sirver/ultisnips' 
" Plug 'scrooloose/syntastic'
" Plug 'bling/vim-airline'
" Plug 'vimoutliner/vimoutliner'
" Plug 'LaTeX-Box-Team/LaTeX-Box'
" Plug 'vim-scripts/Align'

" On-demand loading
"Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" Unmanaged plugin (manually installed and updated)
" Plug '~/my-prototype-plugin'
Plug '/media/dutch/media/Insync/Stuff/Config/vimfiles'

call plug#end()


