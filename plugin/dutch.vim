"	*dutch.vim*	Initialization file
"
" Maintainer:	Greg Matous <gregory.matous@gmail.com>
" Last change:	2014 Dec 11
"
"Set below in _vimrc (set local before global):
"set runtimepath=/media/dutch/media/Insync/Stuff/Config/vimfiles,$VIMRUNTIME
"let VIMCONFIGPATH=$HOME . '/media/dutch/media/Insync/Stuff/Config/vimfiles'

"let test=confirm('python ' . shellescape(VIMCONFIGPATH . '\etc\pydoc3'))
" 
" Debugging:
" use :scriptnames to see what is loaded
" remove ctl-M :%s/^V^M//g

" ========== Compatible behavior =============
" ============================================
set nocompatible

" ========== Basic checks =============
let BACKUPDIR= "$HOME/tmp"
if ! isdirectory(expand("$HOME/tmp"))
	echo "Directory " . expand("$HOME/tmp") . " does not exist."
endif
"
"
"			Use windows copy/paste? (^X, ^C, ^V)
source $VIMRUNTIME/mswin.vim
behave mswin
" use visual mode, not select mode with mouse
set selectmode=""
set mouse=ar
"	Use alt keys for menus in windows?
set winaltkeys=menu

" Use `set guifont` to show current font

if has("unix")
  set guifont=Inconsolata\ Medium\ 12
else
  set guifont=Consolas:h11,Lucida\ Console:h11,Inconsolata:h11
endif

set lines=53 columns=99
winpos 0 0 " set location of window at origin
try
    colorscheme solarized
	set background=dark
catch /^Vim\%((\a\+)\)\=:E185/
    echo "Could not load colorscheme"
endtry
"colorscheme morning " use a colorscheme to set a lot of highlight colors
"highlight Pmenu guifg=darkgreen guibg=lightgray 

" ========== ABbreviations ==========
" ===================================
""ab debug System.out.println("  "); // debug
iab timestamp  <C-R>=strftime("<Last modified by gmmatous on %m/%d/%y %H:%M> " )<CR>

" ========== Settings ==========
" ==============================

" Turn on auto file detection, and load corresponding plugin and indent rules
"filetype off " force reload
"filetype plugin indent on

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
"if &t_Co > 2 || has("gui_running")
"  syntax on
"  set hlsearch
"end

set backup		" keep a backup file
set backupdir=$HOME/tmp,. " where to save backup files
"set diffexpr=MyDiff() " if not set, will use external diff
set completeopt+=longest
set cryptmethod=blowfish " requires vim 7.3
set dir=$HOME/tmp,. " where to keep swap files
set foldmarker={{{,}}} " characters to deliniate folds
set foldexpr=FoldExpression()
set foldmethod=indent " method to determine location of folds
set nofoldenable " turn off folds
set hidden " if true, do not remove abandoned buffers
set hlsearch " highlight search matches
set history=150		" keep 150 lines of command line history
"set ignorecase " ignores the case in search patterns
set laststatus=2 " always show statusline
set listchars=tab:>-,trail:-
set noerrorbells " damn this beep!  ;-)
"       path:   The list of directories to search with an edit command.
set path=.,$VIM
set report=0 " 0=always show a report when N lines were changed.
set ruler		" show the cursor position all the time
set scrolloff=2 " maintain context around cursor
set shiftwidth=4 "Spaces to use for each insertion of (auto)indent.
set shortmess=aIoOtT
set showcmd "Show current uncompleted command?  Absolutely!
set showfulltag "Show search pattern with tag name
set showmatch "Show the matching bracket for the last ')'?
set showmode " Show the current mode?  YEEEEEEEEESSSSSSSSSSS!
set smartcase "	Override 'ignorecase' if search pattern contains uppercase 
set statusline=
set statusline+=%7*\[%n]                                  "buffernr
set statusline+=%1*\ %<%F\                                "File+path
set statusline+=%2*\ %y\                                  "FileType
set statusline+=%3*\ %{''.(&fenc!=''?&fenc:&enc).''}      "Encoding
set statusline+=%3*\ %{(&bomb?\",BOM\":\"\")}\            "Encoding2
set statusline+=%4*\ %{&ff}\                              "FileFormat (dos/unix..) 
set statusline+=%8*\ %=\ row:\                            "Rownumber..
set statusline+=%7*%l\                                     "Rownumber
set statusline+=%8*/%L\ (%03p%%)\                          "/total (%)
set statusline+=%9*\ col:%03c\                            "Colnr
set statusline+=%0*\ \ %m%r%w\ %P\ \%{v:register}          "Modified? Readonly? Top/bot.
set switchbuf=useopen,usetab,newtab " control switching between tabs
set tags=./tags,tags " where are the tags?
set tabstop=4 " How many spaces does a tab represent?
set sessionoptions+=tabpages,resize,winpos,unix,slash
" save uppercase global variables,registers,buffer list, previously edited files. Also saves search patterns, command line, input line ~ history 
"set verbose=20
set undodir=$HOME/tmp
set undofile
set undolevels=1000 "maximum number of changes that can be undone
set undoreload=10000 "maximum number lines to save for undo on a buffer reload
set viminfo=!,\"50,%,'30,h

"" ========= Highlights =============
hi User1 guifg=#ffdad8  guibg=#880c0e
hi User2 guifg=#000000  guibg=#F4905C
hi User3 guifg=#292b00  guibg=#f4f597
hi User4 guifg=#112605  guibg=#aefe7B
hi User5 guifg=#051d00  guibg=#7dcc7d
hi User7 guifg=#ffffff  guibg=#880c0e gui=bold
hi User8 guifg=#ffffff  guibg=#5b7fbb
hi User9 guifg=#ffffff  guibg=#810085
hi User0 guifg=#ffffff  guibg=#094afe

"" ========== AUtocommands ==========
"" ==================================
"
" To debug, set verbose >= 9, and maybe set verbosefile.
"
" Only do this part when compiled with support for autocommands.
if has("autocmd")
  " Put these in an autocmd group, so that we can delete them easily.
  augroup properties
 au!
 " Here is where we can set up our own types

  " For all text files set 'textwidth' to 78 characters.
  autocmd FileType text setlocal textwidth=78
  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif
  augroup END
else
  set autoindent		" always set autoindenting on
endif " has("autocmd")
"
" AUtocommand to set up formatting for Java
 augroup jprog
  " Remove all jprog autocommands
  au!
 " Java specific settings
" See syntax.txt, and syntax/java.vim
let java_highlight_debug=1
let java_highlight_functions="style"
augroup END

" ========== Menus ===========================
" ============================================
"
" == Utilities ==
100amenu &Utilities.&LastModified<Tab> :call mylibrary#LastModified()<CR>
"amenu &Utilities.&Gen_Tags :! ctags --java-types=cmf -f c:/temp/tags -R C:/ATHY/ajb<CR>
amenu &Utilities.&Background :call mylibrary#Background()<CR>
amenu &Utilities.&SpellCheck<Tab> :!C:\Apps\Aspell\bin\aspell.exe check %<CR>:e! %<CR>
amenu &Utilities.&DiffOrig :DiffOrig<CR>

nmenu &Utilities.-Sep2- :

amenu &Utilities.&RunInConque<Tab>F11 <CR>
amenu &Utilities.&BufferExplore :MBEToggle<CR>
amenu &Utilities.&TailMe :Tail %<CR>
amenu &Utilities.&Quicklaunch :Explore $HOME/Application Data/Microsoft/Internet Explorer/Quick Launch<CR>
"amenu &Utilities.&Clojure :ConqueTermSplit java -cp .;C:/lib/clojure/incanter.jar jline.ConsoleRunner clojure.main<CR>
"
" == Plugins ==
120amenu &Plugin.&MRU<Tab>F10 :GundoToggle<CR>
amenu &Plugin.&YankRing<Tab>F9 :YRShow<CR>
amenu &Plugin.&Calendar :Calendar<CR>
amenu &Plugin.&TagList :TlistToggle<CR>
amenu &Plugin.&Project<Tab>F12 :Project<CR>
amenu &Plugin.&ConquePy :ConqueTermSplit python<CR>

" Help menu
amenu &Help.&Helptags :lcd %:p:h . <CR> . :helptags . <CR>
amenu &Help.&Markdown :help markdown<CR>
amenu &Help.&QuickRef :help dutch<CR>
"
" Toolbar with popups
amenu 1.395 ToolBar.-sep8-		<Nop>
amenu 1.400 ToolBar.Shell		 :silent sh<CR>
tmenu ToolBar.Shell Run a Shell
amenu 1.405 ToolBar.WinSplit	<C-W>s
tmenu ToolBar.WinSplit Split Horizontally
amenu 1.410 ToolBar.WinVSplit	<C-W>v
tmenu ToolBar.WinVSplit Split Vertically

"" ========== MAPpings ==========
"" ==============================
nmap <ESC><ESC> <C-W>c
" function keys
"map! <F1> for help
noremap <F2> :Vexplore <CR>
noremap <F2> :call NetrwToggle(getcwd(), 0)<CR>
noremap <F3> :call NetrwToggle("", 0)<CR>

"noremap <F4>  " quickbuffer

noremap <F5> :BufExplorerVerticalSplit <CR>
noremap <F6> :GundoToggle <CR>
noremap <F7> :Vexplore <CR>
noremap <F8> :Vexplore <CR>
noremap <F9> :Vexplore <CR>
noremap <F10> :GundoToggle <CR>
noremap <F11> :Vexplore <CR>
" map! <F12> :Vexplore <CR>

" Cycle thru buffers and jumps
map <S-Right> :bnext<CR>
map <S-Left> :bprev<CR>
map <S-Up> :tabnext<CR> 
map <S-Down> :tabprevious<CR>

" map so dont have to reach for esc
" can map caps lock to control at OS
imap <S-Space> <ESC>
nmap <S-Space> :nohlsearch<CR>:update<CR>
imap <C-Space> <ESC>:nohlsearch<CR>:update<CR>
imap <S-Return> <ESC><Bslash><Bslash>
imap <S-Return> <Bslash><Bslash>

" make it easy to get to beginning/end of line in insert mode
map! <C-UP> <ESC>^i
map! <C-DOWN> <ESC>$a
map! <C-LEFT> <ESC>bi
map! <C-RIGHT> <ESC>wa

" close a tag
map! <C-B> <esc>byeea></<esc>pa><esc>3bi<<esc>2wa
"" remap it to correct this common typo:
nmap :W :w

"" VIM - Editing and updating the vimrc:
"" As I often make changes to this file I use these commands
"" to start editing it and also update it:
if has("unix")
  let vimrc='~/.vimrc'
else
"" ie:  if has("dos16") || has("dos32") || has("win32")
  let vimrc='$VIM\_vimrc'
endif
  nn  ,u :source <C-R>=vimrc<CR><CR>
  nn  ,v :edit   <C-R>=vimrc<CR><CR>

 
map <silent> <C-F2> :if &guioptions =~# 'T' <Bar>
                         \set guioptions-=T <Bar>
                         \set guioptions-=m <bar>
                    \else <Bar>
                         \set guioptions+=T <Bar>
                         \set guioptions+=m <Bar>
                    \endif<CR> 

" ========== Functions ==========
" ===============================
" 

" do a completion if there are not pure spaces before cursor
function! CleverTab()
   if strpart( getline('.'), 0, col('.')-1 ) =~ '^\s*$'
      return "\<Tab>"
   else
      return "\<C-N>"
endfunction
inoremap <Tab> <C-R>=CleverTab()<CR>

"see the differences between the current buffer and the file it was loaded from.
" 
command! DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
			\ | wincmd p | diffthis

"com!  -nargs=* -bar -bang -complete=dir  NetrwToggle  call NetrwToggle(<q-args>, <bang>0)

 " === Toggle netrw ===
" ===============================
fun! NetrwToggle(dir, right)
  if exists("t:netrw_lexbufnr")
  " close down netrw explorer window
  let lexwinnr = bufwinnr(t:netrw_lexbufnr)
  if lexwinnr != -1
    let curwin = winnr()
    exe lexwinnr."wincmd w"
    close
    exe curwin."wincmd w"
  endif
  unlet t:netrw_lexbufnr

  else
    " open netrw explorer window in the dir of current file
    " (even on remote files)
    let path = substitute(exists("b:netrw_curdir")? b:netrw_curdir : expand("%:p"), '^\(.*[/\\]\)[^/\\]*$','\1','e')
    exe (a:right? "botright" : "topleft")." vertical ".((g:netrw_winsize > 0)? (g:netrw_winsize*winwidth(0))/100 : -g:netrw_winsize) . " new"
    if a:dir != ""
      exe "Explore ".a:dir
    else
      exe "Explore ".path
    endif
    setlocal winfixwidth
    let t:netrw_lexbufnr = bufnr("%")
  endif
endfun


"set diffexpr=MyDiff()
function! MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '""' . $VIMRUNTIME . '\diff"'
      let eq = '"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction
" ========== Plugin Settings ==========
" =====================================
" 
" === EasyMotion ===
" let g:EasyMotion_leader_key = '\' , default = '\\'
" map <Leader> <Plug>(easymotion-prefix)

" === netrw builtin (see netrw browser variables ===

" sort is affecting only: directories on the top, files below
let g:netrw_sort_sequence = '[\/]$,*'
let s:dotfiles = '\(^\|\s\s\)\zs\.\S\+'

let g:netrw_sort_sequence = '[\/]$,*,\%(' . join(map(split(&suffixes, ','), 'escape(v:val, ".*$~")'), '\|') . '\)[*@]\=$'
let s:escape = 'substitute(escape(v:val, ".$~"), "*", ".*", "g")'
" comma-separated pattern for hiding files
let g:netrw_list_hide =
      \ join(map(split(&wildignore, ','), '"^".' . s:escape . '. "$"'), ',') . ',^\.\.\=/\=$' .
      \ (get(g:, 'netrw_list_hide', '')[-strlen(s:dotfiles)-1:-1] ==# s:dotfiles ? ','.s:dotfiles : '')
let g:netrw_banner = 0
let s:netrw_up = ''
let g:netrw_browse_split=4  " open in previous window (prevents overpopulation of splits)
let g:netrw_home=$HOME  " where bookmarks/history are saved
let g:netrw_keepdir=0 " change current directory same as browsing dir
let g:netrw_localrmdir="echo 'not supported' "
let g:netrw_winsize   = 30
let g:netrw_liststyle=0         " thin (change to 3 for tree)
let g:netrw_banner=0            " no banner
let g:netrw_altv=1              " open files on right
let g:netrw_preview=1           " open previews vertically

" === bufexplorer plugin ===

let g:bufExplorerDefaultHelp=1       " Show default help.

let g:bufExplorerDisableDefaultKeyMapping=1    " Disable mapping.

let g:bufExplorerFindActive=1        " Go to active window.

let g:bufExplorerShowDirectories=0   " Show directories in listing.

let g:bufExplorerShowNoName=0        " Do not show No Name buffers.

let g:bufExplorerShowRelativePath=1  " Show relative paths.

let g:bufExplorerSortBy='mru'        " Sort by most recently used.

let g:bufExplorerSplitHorzSize=30     " New split window is n rows high.

let g:bufExplorerSplitOutPathName=1  " Don't Split the path and file name.

let g:bufExplorerSplitRight=0        " Split left.

let g:bufExplorerSplitVertSize=30          " New split window is n columns wide.

" === supertab plugin ===
let g:SuperTabDefaultCompletionType = "<C-X><C-N>"
let g:SuperTabNoCompleteAfter = ['^', ',', '\s']

" === Project plugin ===
let g:proj_window_width=15
let g:proj_window_increment=25
" n=number, c=autoclose, T = put folds at top
let g:proj_flags='gmsStv'
let g:proj_run2='silent !start c:\Apps\IrfanView\i_view32.exe %f' 

" === Tail plugin ===
let g:tail#Height=48

" === Calendar plugin ===
let g:calendar_diary = 'C:/tmp'

" === YankRing plugin ===
let g:yankring_default_menu_mode = 0
"
" === Quickbuffer plugin ===
let g:qb_hotkey = "<F4>"

" ==============================================
" === Settings for Conque ===



