" Vim filetype plugin file
" Language:	notes
" Maintainer:  Gregory Matous	
" Last Change:	2008 Feb 20

" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

let b:undo_ftplugin = "setl modeline< tw< fo<"
setlocal foldmethod=expr foldminlines=4 foldcolumn=2
"This will make a fold out of paragraphs separated by blank lines: >
"setlocal foldexpr=getline(v:lnum)=~'^\\s*$'&&getline(v:lnum+1)=~'\\S'?'<1':1

setlocal foldmethod=marker
setlocal foldminlines=1
setlocal foldopen=all
setlocal foldclose=all

colorscheme desert

" ========== Settings ==========
" ==============================
setlocal textwidth=0
setlocal wrapmargin=0 "Number of cols from border to break line
setlocal wrap  "lines will visually wrap if > width of window
setlocal linebreak  "wrap at words not chars (defined by breakat)
setlocal formatoptions=2 "recognize numbered lists and not break long lines.
setlocal autoindent "Copy indent from current line when starting a new line

setlocal thesaurus+=c:/lib/mthesaur.txt
setlocal spell spelllang=en_us
"" ========== MAPpings ==========
"" ==============================


