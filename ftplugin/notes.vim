" Vim filetype plugin file
" Language:	notes
" Maintainer:  Gregory Matous	
" Last Change:	2008 Feb 20

" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

let b:undo_ftplugin = "setl modeline< tw< fo<"
setlocal foldexpr=NotesFoldExpression()
setlocal foldmethod=expr foldminlines=4 foldcolumn=2
setlocal conceallevel=1
colorscheme murphy

" ========== Menus ===========================
" ============================================
"
amenu &Utilities.&Move2End<Tab>C-F3 <C-F3>
amenu &Utilities.&Copy2End<Tab>C-F4 <C-F3>

" ========== Settings ==========
" ==============================
" Can wrap text using either margins or hard width
setlocal textwidth=0
setlocal wrapmargin=0 "Number of cols from border to break line
setlocal wrap  "visually wrap
setlocal linebreak  "wrap at words not chars (defined by breakat)
setlocal formatoptions=tcqwn "recognize numbered lists.
setlocal autoindent "Copy indent from current line when starting a new line
" set showbreak "+++ " shows lines that have wrapped.
" set breakat " tells what chars lines will wrap

"" ========== MAPpings ==========
"" ==============================
"" mappings for todo lists
nmap <F2> ddGpA<C-R>=strftime(" <Done %m/%d/%y> " )<CR><ESC>''zz
nmap <F3> yyGpA<C-R>=strftime(" <Done %m/%d/%y> " )<CR><ESC>''zz

" Add mappings, unless the user didn't want this.
if !exists("no_plugin_maps") && !exists("no_mail_maps")
  " Quote text by inserting "> "
  if !hasmapto('<Plug>MailQuote')
    vmap <buffer> <LocalLeader>q <Plug>MailQuote
    nmap <buffer> <LocalLeader>q <Plug>MailQuote
  endif
  vnoremap <buffer> <Plug>MailQuote :s/^/> /<CR>:noh<CR>``
  nnoremap <buffer> <Plug>MailQuote :.,$s/^/> /<CR>:noh<CR>``
endif

function! NotesFoldExpression()
if getline(v:lnum+1)=~'.*:$' 
let s:expr = '<1'
else
  if getline(v:lnum)=~'^>>.*$'
	 let s:expr = '0'
  else
	 let s:expr = '1'
  endif
return s:expr
endfunction

