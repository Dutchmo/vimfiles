"
"Because these are autoload functions, they can be declared by plugins,
"but are only actually loaded when called.
"
" ========== Functions ==========
" ===============================
" 
function mylibrary#Background()
if (&bg=='light')
	highlight Normal guifg=seagreen
	highlight Normal guibg=black
	set bg=dark
	syntax on
else
	highlight Normal guifg=black
	highlight Normal guibg=grey90
	set bg=light
	syntax on
endif
endfunction

"command! -nargs=* Format call mylibrary#Format(<q-args>)

function mylibrary#Prop()
let cur = line(".")
let line = getline(line("."))
let line = substitute(line, "private", '','') " remove keyword
let line = substitute(line, ';$','','') " remove trailing semicolon
if (line =~ "[]")
  let array = " [] "
  let line = substitute( line, '\s*\[\]\s*', ' ', '' ) " remove brackets 
else
  let array = " "
endif
let line = mylibrary#Trim(line) " remove leading and trailing spaces
let index = matchend(line, '\w*')
let type = strpart(line, 0, index)
let name = strpart(line, index+1, strlen(line))
let output =  "public void set" . mylibrary#InitCaps(name) . "(".type.array.name.") {"
call append(cur, output)
let output = "this." . name . " = " . name . ";"
call append(cur+1, output)
call append(cur+2, "}")
let output = "public " . type . array . "get".mylibrary#InitCaps(name)."() {"
call append(cur+3, output)
let output = "return this." . name . ";"
call append(cur+4, output)
call append(cur+5, "}")
if (strlen(array) > 1) " if array property
  let output = "public void set".mylibrary#InitCaps(name)
  let output = output . "(int index, ".type." ".name.") {"
  call append(cur+6, output)
  let output = "this.".name."[index] = ".name.";"
  call append(cur+7, output)
  call append(cur+8, "}")
  let output = "public ".type." get".mylibrary#InitCaps(name)."(int index) {"
  call append(cur+9, output)
  let output = "return this.".name."[index];"
  call append(cur+10, output)
  call append(cur+11, "}")
  call mylibrary#Format(cur, cur+12)
else
	call mylibrary#Format(cur, cur+6)
endif
endfunction


function mylibrary#Trim(str)
l"t s:line = a:str
let s:line = substitute( s:line, '^\s\+', '', 'g' )
let s:line = substitute( s:line, '\s\+$', '', '' )
return s:line
endfunction

function mylibrary#InitCaps(str)
return substitute(a:str, ".", "\\U\\0", "")
let s:ascii = char2nr(a:str)
if (s:ascii < 97)
	echo " Not a valid lowercase variable."
	return -1
endif
let char1 = nr2char(ascii - 32)
return char1 . strpart(a:str, 1, strlen(a:str))
endfunction

function mylibrary#BlockOut(str)
	let cur = line(".")
let line = getline(line("."))
let tot_len=70
let com_len = strlen(line)
let pre = (tot_len-com_len)/2 - 1
exe "normal! A \<ESC>0" . pre .  "i".a:str."\<ESC>a \<ESC>" . pre . "A".a:str 
	exe "normal! O\<ESC>".tot_len."i-\<ESC>jo\<ESC>".tot_len."i-"
endfunction

function mylibrary#MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '""' . $VIMRUNTIME . '\diff"'
      let eq = '"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction

function mylibrary#FoldExpression()
if getline(v:lnum)=~'^\s*$' && getline(v:lnum+1) =~ '\S'
let s:expr = '<1'
  else
	 let s:expr = '1'
  endif
return s:expr
endfunction

" If buffer modified, update any 'Last modified: ' in the last 2 lines.
" 'Last modified: ' can have up to 10 characters before (they are retained).
" Restores cursor and window position using save_cursor variable.
" Don't need ! since it should not already exist.
function mylibrary#LastModifiedUpdate()
if &modified
  let save_cursor = getpos(".")
  let n = line("$")
  let user=$USER
  exe (n-1) . ',' . n . 's#^\(.\{,10}<Last modified on \).*#\1' .
          \ strftime('%m/%d/%y %H:%M') . ' by ' . user . '>' . '#e'
  call setpos('.', save_cursor)
endif
endfunction

function mylibrary#LastModified()
  if &modified
    let save_cursor = getpos(".")
    let n = line("$")
    let user=$USER
    let result = '<Last modified on ' . strftime('%m/%d/%y %H:%M') . ' by ' . user . '>'
    call append(n, result)
    call setpos('.', save_cursor)
  endif
endfunction

