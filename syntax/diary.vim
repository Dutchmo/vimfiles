" Vim syntax file
" Language:		Diary syntax file
" Maintainer:		Gregory "Dutch" Matous
" Last Change:		<Modified by gmmatous on 10/26/09 12:07> 
" Example: " (xx), <xx>, >>xx, *xx *, =xx =, __xx __, xx:, 123)

" Quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif
" read in the Notes syntax
:source <sfile>:h/notes.vim

syn match nFold "^{{{.*$"
syn match nFoldClose "^.*}}}$"

" Define the default highlighting.
hi def link nFold Label
hi def link nFoldClose Label

let b:current_syntax = "diary"
" below are groups representing the various types of highlighting
" String Identifier Label Define Type Special Underlined Ignore Error Todo
