" Vim syntax file
" Language:		Notes syntax file
" Maintainer:		Gregory "Dutch" Matous
" Last Change:	<Modified by gmmatous on 10/26/09 12:08> 	
" Example:  " (xx), <xx>, >>xx, *xx *, =xx =, __xx __, xx:, 123)

" Quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

syn keyword	nTodo TODO Todo 
syn keyword nError error Error ERROR exception Exception

syn region	nQuote start=+L\="+ skip=+\\\\\|\\"+ end=+"+ 
syn region nParens start="(" end=")" contains=nTodo
syn region nHeader start="^\s*at\s" end=")$" contains=nError 
syn region nUnderline start="__" end="__" contains=nTodo
syn match nLabel "^.*:$"
syn match nUmbered "^\d+\s"
syn match nAside +\[slf5s.\w\+\]+

"  Comment, Define, Error,Identifier, Label,Todo, Type, Special, String,
"  Underlined
" Define the default highlighting.
hi def link nHeader Type
hi def link nQuote String
hi def link nLabel Label
hi def link nTodo	Todo
hi def link nParens Identifier 
hi def link nUnderline Underlined
hi def link nUmbered Define
hi def link nError Error
hi Type gui=italic,bold

let b:current_syntax = "log"
setlocal foldmethod=manual
execute 'normal G'
" below are groups representing the various types of highlighting
" String Identifier Label Define Type Special Underlined Ignore Error Todo
