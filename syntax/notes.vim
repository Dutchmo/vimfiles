" Vim syntax file
" Language:		Notes syntax file
" Maintainer:		Gregory "Dutch" Matous
" Last Change:	<Modified by gmmatous on 10/26/09 12:08> 	
" Example:  " (xx), <xx>, >>xx, *xx *, =xx =, __xx __, xx:, 123)

" Quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

syn keyword	nTodo TODO Todo 

syn region	nQuote start=+L\="+ skip=+\\\\\|\\"+ end=+"+ 
syn region nParens start="(" end=")" contains=nTodo
syn region nAside start="<" end=">" contains=nTodo
syn region nDefine start=">>" end="$" 
syn region nHeader start="^\s*@\s" end="@\s*$" contains=nTodo
syn region nHeader2 start="^\s*=\+\s" end="=\+\s*$" contains=nTodo
syn region nUnderline start="__" end="__" contains=nTodo
syn match nLabel "^.*:$"
syn match nUmbered "^\d\{1,2})\s"
syn match nPword "[Hh]c\w\+" conceal cchar=X "see :digraphs for others
syn match nPword "pword:\w\+" conceal cchar=X "see :digraphs for others

"  Comment, Define, Error,Identifier, Label,Todo, Type, Special, String,
"  Underlined
" Define the default highlighting.
hi def link nAside Comment 
hi def link nDefine Define 
hi def link nHeader Identifier
hi def link nHeader2 Special
hi def link nQuote String
hi def link nLabel Label
hi def link nTodo	Todo
hi def link nParens Type 
hi def link nUnderline Underlined
hi def link nUmbered Define
hi def link nPword Conceal
hi Type gui=italic,bold

"" ========= Highlights =============
hi User1 guifg=#ffdad8  guibg=#880c0e
hi User2 guifg=#000000  guibg=#F4905C
hi User3 guifg=#292b00  guibg=#f4f597
hi User4 guifg=#112605  guibg=#aefe7B
hi User5 guifg=#051d00  guibg=#7dcc7d
hi User7 guifg=#ffffff  guibg=#880c0e gui=bold
hi User8 guifg=#ffffff  guibg=#5b7fbb
hi User9 guifg=#ffffff  guibg=#810085
hi User0 guifg=#ffffff  guibg=#094afe


let b:current_syntax = "notes"
" below are groups representing the various types of highlighting
" String Identifier Label Define Type Special Underlined Ignore Error Todo
