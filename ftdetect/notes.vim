
augroup notesdetect
 autocmd BufRead,BufNewFile *.notes,*.not	setfiletype notes
augroup END

augroup diarydetect
 autocmd BufRead,BufNewFile *.diary,*.dry	setfiletype diary
augroup END

augroup notestimestamp
	autocmd BufWritePre *.diary,*.notes call mylibrary#LastModifiedUpdate()
augroup END

